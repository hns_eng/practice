Adapted from [here](https://github.com/grayghostvisuals/practice-git)

### Git Your Practice On!

* Git Reference [https://help.github.com/categories/github-pages-basics/](https://help.github.com/categories/github-pages-basics/)
* Pro Git Online Book [http://git-scm.com/book](http://git-scm.com/book)
* Git Ready [http://gitready.com](http://gitready.com)
* Quick Command Practice [http://try.github.com](http://try.github.com)
* Git Real [http://www.codeschool.com/courses/git-real](http://www.codeschool.com/courses/git-real)
* How to GitHub: Fork, Branch, Track, Squash and Pull Request [http://gun.io/blog/how-to-github-fork-branch-and-pull-request](http://gun.io/blog/how-to-github-fork-branch-and-pull-request)
* Learn Git Online [http://learn.github.com/p/intro.html](http://learn.github.com/p/intro.html)
* Teach Github [https://github.com/github/teach.github.com](https://github.com/github/teach.github.com)
* Git: The Simple Guide [http://rogerdudler.github.com/git-guide](http://rogerdudler.github.com/git-guide)
* Git Immersion [http://gitimmersion.com](http://gitimmersion.com)
* Git Branching [http://pcottle.github.io/learnGitBranching/](http://pcottle.github.io/learnGitBranching/)
* Git Cheat Sheet [https://www.atlassian.com/git/tutorials/atlassian-git-cheatsheet](https://www.atlassian.com/git/tutorials/atlassian-git-cheatsheet)

Welcome to this practice git repository where you can mess up as much as you'd like plus work with a real, living, breathing person on the other side.
Here we learn all things git. In this repository we make feature branches and submit Pull Requests just to discover what it's like when a repo owner asks you

> Can you squash/rebase/amend your commits for us

and you're all like...

> How do I do that?

This is where we make those mistakes. See if you can create horrific merge conflicts to see how git deals with them. Then try doing things as cleanly as possible. Try to break the master branch. Try making release branches. Try supporting a super old branch. Test the limits of your power!

### Instructions

* Clone this repo
* Create a feature branch off of master
* Name the feature branch `<your initials>/short_description_here`
* Push the branch to BitBucket
* Submit a Pull Request with your feedback
* Review feedback and fix any issues
* Get your branch merged with master

What the feature branch does is up to you. Feel free to:

* fixtypos in the raedme.
* Enhance the readme to be more helpful
* refactor some arbitrary function in the source files.
* fix a bug from the issue tracker
* add more comments to the code
* add a python utility that does something
* delete files
* request branch protections

#### Typical & Highly Useful Git Commands

At a bare minimum, you'll need to learn basic usage of the following git commands (using links at the top):

* git clone
* git status
* git log
* git add
* git commit
* git branch
* git checkout
* git push
* git pull

For extra credit, optionally learn basic usage of the following:

* git rebase
* git difftool
* git mergetool
