# Contributing

When contributing to this repository, please adhere to the following guidelines:

## Git Guidelines

### Push Atomic Commits

Atomic commits mean that the commit only contains one logical change. For example,
a commit containing a bug fix and a commit containing a new feature are examples
of atomic commits. A commit containing *both* a bug fix *and* a new feature is
not an atomic commit.

### Push Stable Commits

It is good practice to commit often to your local repo to save changes that you've made. However,
sometimes these commits leave the repository temporarily unstable. A later local commit
may fix this issue. Before pushing to the remote repository, please *squash*
the unstable commit(s) together so that only stable commits are pushed.

### Rebase Local Commits

If developing on the same branch you are pulling from, please `git pull --rebase` your commits
on top of the pulled commits. We want to keep the history as free from noise as
possible.

### Merge Local Branches

If merging a local branch in with master, use `git merge` (not rebase) so that we can see in
the history that a branch was merged.

### Commit Messages

One line commit messages are insufficient for anything but the simpliest of changes.

Please use the following template when committing:

```
One line summary

Description:

Testing:

```

If you need help remembering, you can set your `commit.template` git setting to
a file containing that template.


## C++ Guidelines

### Whitespace

Files shall use 4 spaces for tabbing

### CppCheck

All C++ code shall through cppcheck before committing. If you need help remembering
please set a pre-commit hook to automatically do it for you.
